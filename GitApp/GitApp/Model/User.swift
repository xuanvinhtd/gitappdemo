//
//  User.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import Foundation

struct User: Codable {
    var id: Int
    var login: String?
    var avatar_url: String?
    var html_url: String?
    var location:String?
    var bio:String?
    var public_repos:Int?
    var followers:Int?
    var following:Int?
    
    init(user :UserGit) {
        self.id = Int(user.id)
        self.login = user.login
        self.avatar_url = user.avatar_url
        self.html_url = user.html_url
        self.location = user.location
        self.bio = user.bio
        self.public_repos = Int(user.public_repos)
        self.followers = Int(user.followers)
        self.following = Int(user.following)
    }
}
