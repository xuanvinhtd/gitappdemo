//
//  UserDetailViewController.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import UIKit

class UserDetailViewController: UIViewController {
    //MARK: - Properties
    var userViewModel:UserViewModel!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(UserDetailViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    //MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userViewModel.getDetail { [weak self] in
            self?.tableView.reloadData()
        }
        
        self.title = "Profile"
        self.tableView.tableFooterView = UIView()
        self.tableView.refreshControl = refreshControl
        
        self.tableView.register(UINib(nibName: "UserInfoTableViewCell", bundle: nil), forCellReuseIdentifier: UserInfoTableViewCell.reuseIdentifier)
        self.tableView.register(UINib(nibName: "AboutTableViewCell", bundle: nil), forCellReuseIdentifier: AboutTableViewCell.reuseIdentifier)
        self.tableView.register(UINib(nibName: "StatusTableViewCell", bundle: nil), forCellReuseIdentifier: StatusTableViewCell.reuseIdentifier)
    }
    
    deinit {
        print("denit UserDetailViewController")
    }
    
    //MARK: - Handle event
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.userViewModel.getDetail { [weak self] in
            self?.tableView.reloadData()
            
            if self?.refreshControl.isRefreshing ?? false {
                self?.refreshControl.endRefreshing()
            }
        }
    }
}

////MARK: - UITableViewDelegate
extension UserDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 1 {
            return 100
        } else {
            return 150
        }
    }
}

//MARK: - UITableViewDataSource
extension UserDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = userViewModel.user
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: UserInfoTableViewCell.reuseIdentifier) as? UserInfoTableViewCell
            cell?.userInfo = item
            return cell!
        }
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: AboutTableViewCell.reuseIdentifier) as? AboutTableViewCell
            cell?.about.text = item.bio
            return cell!
        }
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: StatusTableViewCell.reuseIdentifier) as? StatusTableViewCell
            cell?.publicRepoLabel.text = "\(item.public_repos ?? 0)"
            cell?.followesLabel.text = "\(item.followers ?? 0)"
            cell?.followingLabel.text = "\(item.following ?? 0)"
            return cell!
        }
        
        return UITableViewCell(style: .default, reuseIdentifier: "DefaultCell")
    }
}

