//
//  UserListViewController.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import UIKit

class UserListViewController: UIViewController {
    //MARK: - Properties
    private var userListViewModel:UserListViewModel!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(UserListViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    //MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userListViewModel = UserListViewModel()
        self.userListViewModel.loadSource { [weak self] in
           self?.tableView.reloadData()
        }
        
        self.tableView.tableFooterView = UIView()
        self.tableView.refreshControl = refreshControl
        self.tableView.register(UINib(nibName: "UserInfoTableViewCell", bundle: nil), forCellReuseIdentifier: UserInfoTableViewCell.reuseIdentifier)
    }
    
    //MARK: - Handle event
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.userListViewModel.loadSource { [weak self] in
            self?.tableView.reloadData()
            
            if self?.refreshControl.isRefreshing ?? false {
                self?.refreshControl.endRefreshing()
            }
        }
    }
}

//MARK: - UITableViewDelegate
extension UserListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "UserDetailViewController") as! UserDetailViewController
        vc.userViewModel = userListViewModel.user(at: indexPath.row)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - UITableViewDataSource
extension UserListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userListViewModel.usersViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UserInfoTableViewCell.reuseIdentifier) as! UserInfoTableViewCell
        cell.user = userListViewModel.user(at: indexPath.row).user
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
