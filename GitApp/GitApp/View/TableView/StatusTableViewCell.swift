//
//  StatusTableViewCell.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import UIKit

class StatusTableViewCell: UITableViewCell,Reusable {
    //MARK: - Properties
    @IBOutlet weak var publicRepoLabel: UILabel!
    @IBOutlet weak var followesLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    
    //MARK: - Initialization
    override func prepareForReuse() {
        super.prepareForReuse()
        followingLabel.text = "0"
        followesLabel.text = "0"
        publicRepoLabel.text = "0"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
