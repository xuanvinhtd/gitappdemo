//
//  AboutTableViewCell.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import UIKit

class AboutTableViewCell: UITableViewCell,Reusable {
    @IBOutlet weak var about: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        about.text = ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
