//
//  UserInfoTableViewCell.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import UIKit

class UserInfoTableViewCell: UITableViewCell, Reusable {
    //MARK: - Properties
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var urlString: UILabel!
    
    var user :User? {
        didSet {
            guard let u = user else {
                return
            }
            loadImageFrom(url: u.avatar_url ?? "")
            userName.text = u.login
            urlString.text = u.html_url
        }
    }
    
    var userInfo :User? {
        didSet {
            guard let u = userInfo else {
                return
            }
            loadImageFrom(url: u.avatar_url ?? "")
            userName.text = u.login
            urlString.text = u.location
        }
    }
    
    //MARK: - Initialization
    override func prepareForReuse() {
        super.prepareForReuse()
        avatar.image = UIImage(named: "avatarDefault")
        userName.text = ""
        urlString.text = ""
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        avatar.layer.cornerRadius = self.avatar.frame.size.width/2.0
        avatar.layer.masksToBounds = true
    }

    //MARK: - Support function
    private func loadImageFrom(url:String) {
        Helper.downloaded(from: url) { [weak self] (img) in
            guard let strongSelf = self, let newImage = img else { return }
            DispatchQueue.main.async() {
                strongSelf.avatar.image = newImage
                strongSelf.avatar.layer.cornerRadius = strongSelf.avatar.frame.size.width/2.0
                strongSelf.avatar.layer.masksToBounds = true
            }
        }
    }
    
}
