//
//  UserViewModel.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import Foundation

class UserViewModel {
    //MARK: - Properties
    var user :User

    //MARK: - Initialization
    init(user :User) {
        self.user = user
    }
    
    //MARK: - Support function
    func getDetail(completion :@escaping () -> ()) {
        let service = WebService()
        service.getUserInfoWith("\(user.id)") { [weak self] (user) in
            guard let strongSelf = self else { return }
            if let u = user {
                strongSelf.user.bio = u.bio
                strongSelf.user.public_repos = u.public_repos
                strongSelf.user.following = u.following
                strongSelf.user.followers = u.followers
                strongSelf.user.location = u.location
                CacheData.share.save(user: strongSelf.user)
                completion()
            } else {
                print("Load User Info from cache")
                CacheData.share.getUserFor("\(self?.user.id ?? 0)", completion: { (user) in
                    if let u = user {
                       strongSelf.user = u
                    }
                    completion()
                })
            }
        }
    }
}
