//
//  UserViewModel.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import Foundation

class UserListViewModel {
    //MARK: - Properties
    var usersViewModel = [UserViewModel]()
    private var webservice :WebService!
    
    //MARK: - Initialization
    init() {
        self.webservice = WebService()
    }
    
    //MARK: - Support function
    func loadSource(completion :@escaping () -> ()) {
        self.webservice.getUsers { [unowned self] sources in
            if sources.count == 0 {
                print("Load User from cache")
                CacheData.share.getUsersFor(completion: { (users) in
                    self.usersViewModel = users.compactMap(UserViewModel.init)
                    completion()
                })
            } else {
                self.usersViewModel = sources.compactMap(UserViewModel.init)
                CacheData.share.save(users: sources)
                completion()
            }
        }
    }
    
    func user(at index:Int) -> UserViewModel {
        return self.usersViewModel[index]
    }
}
