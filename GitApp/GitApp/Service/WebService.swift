//
//  WebService.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String:Any]
struct WebService {
    typealias Users = [User]
    //MARK: - Request data Function
    func getUsers(completion :@escaping ([User]) -> ()) {
        let url = Configuration.GITApi.getUsers.url()
        URLSession.shared.dataTask(with: url) { data, _, error in
            var sources = [User]()
            if let data = data, data.count != 0 {
                sources = self.parseUsersFrom(data)
                CacheData.share.save(users: sources)
            }
            DispatchQueue.main.async {
                completion(sources)
            }
            }.resume()
    }
    
    func getUserInfoWith(_ idString:String, completion :@escaping (User?) -> ()) {
        let url = Configuration.GITApi.getUserInfoWith(id: idString).url()
        URLSession.shared.dataTask(with: url) { data, _, error in
            var sources:User?
            if let data = data, data.count != 0 {
                sources = self.parseUserInfoFrom(data)
            }
            DispatchQueue.main.async {
                completion(sources)
            }
            }.resume()
    }
    
    //MARK: - Parse data function
    private func parseUsersFrom(_ data:Data) -> [User] {
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(Users.self, from: data)
        } catch let err {
            print("Err", err)
            return [User]()
        }
    }
    
    private func parseUserInfoFrom(_ data:Data) -> User? {
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(User.self, from: data)
        } catch let err {
            print("Err", err)
            return nil
        }
    }
}
