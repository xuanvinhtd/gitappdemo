//
//  CacheData.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import UIKit
import CoreData

final class CacheData {
    //MARK: - Properties
    let cacheInstance = NSCache<NSString, NSData>()
    static let share = CacheData()
    
    //MARK: - Initialization
    private init() {}
    
    //MARK: - Cache function
    func save(users:[User]) {
        DispatchQueue.main.async {
            for item in users {
                self.update(user: item)
            }
        }
    }
    
    func save(user:User) {
        DispatchQueue.main.async {
            self.update(user: user)
        }
    }
    
    func getUserFor(_ id:String, completion :@escaping (User?) -> ()) {
        DispatchQueue.main.async {
            completion(self.getUserGitFor(id: id))
        }
    }
    
    func getUsersFor(completion :@escaping ([User]) -> ()) {
        DispatchQueue.main.async {
            completion(self.getUsersGit())
        }
    }

    //MARK: - CoreData function
    //Add new and update user info
    private func update(user:User) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "UserGit")
        fetchRequest.predicate = NSPredicate(format: "id = %d", user.id)
        
        if let users = try? managedContext.fetch(fetchRequest), users.count > 0 {
            let objectUpdate = users[0] as! NSManagedObject
            if let url = user.avatar_url {
                objectUpdate.setValue(url, forKeyPath: "avatar_url")
            }
            if let location = user.location {
                objectUpdate.setValue(location, forKeyPath: "location")
            }
            if let html_url = user.html_url {
                objectUpdate.setValue(html_url, forKeyPath: "html_url")
            }
            if let bio = user.bio {
                objectUpdate.setValue(bio, forKeyPath: "bio")
            }
            if let login = user.login {
                objectUpdate.setValue(login, forKeyPath: "login")
            }
            if let following = user.following {
                objectUpdate.setValue(following, forKeyPath: "following")
            }
            if let followers = user.followers {
                objectUpdate.setValue(followers, forKeyPath: "followers")
            }
            if let public_repos = user.public_repos {
                objectUpdate.setValue(public_repos, forKeyPath: "public_repos")
            }
        } else {
            let entity = NSEntityDescription.entity(forEntityName: "UserGit", in: managedContext)!
            let userGit = NSManagedObject(entity: entity, insertInto: managedContext)
            userGit.setValue(user.id, forKeyPath: "id")
            if let url = user.avatar_url {
                userGit.setValue(url, forKeyPath: "avatar_url")
            }
            if let html_url = user.html_url {
                userGit.setValue(html_url, forKeyPath: "html_url")
            }
            if let location = user.location {
                userGit.setValue(location, forKeyPath: "location")
            }
            if let bio = user.bio {
                userGit.setValue(bio, forKeyPath: "bio")
            }
            if let login = user.login {
                userGit.setValue(login, forKeyPath: "login")
            }
            if let following = user.following {
                userGit.setValue(following, forKeyPath: "following")
            }
            if let followers = user.followers {
                userGit.setValue(followers, forKeyPath: "followers")
            }
            if let public_repos = user.public_repos {
                userGit.setValue(public_repos, forKeyPath: "public_repos")
            }
        }
        
        self.saveCoreDataOf(managedContext)
    }

    private func getUsersGit() -> [User] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return []
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserGit")
        
        do {
            let users = try managedContext.fetch(fetchRequest)
            return users.map { User(user: $0 as! UserGit) }
        } catch {
            print("Cache error - \(error)")
            return []
        }
    }
    
    private func getUserGitFor(id:String) -> User? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "UserGit")
        fetchRequest.predicate = NSPredicate(format: "id = %@", id)
        
        do {
            let users = try managedContext.fetch(fetchRequest)
            return users.map { User(user: $0 as! UserGit) }.first
        } catch {
            print("Cache error - \(error)")
            return nil
        }
    }
    
    private func saveCoreDataOf(_ managedContext: NSManagedObjectContext) {
        do {
            try managedContext.save()
        } catch {
            print("Cache error - \(error)")
        }
    }
}
