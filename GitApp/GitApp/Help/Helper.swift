//
//  Helper.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import UIKit


struct Helper {
    static func downloaded(from url: URL, completion :@escaping (UIImage?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                completion(nil)
                return
            }
            DispatchQueue.main.async() {
                completion(image)
            }
            }.resume()
    }
    static func downloaded(from link: String, completion :@escaping (UIImage?) -> ()) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url,completion: completion)
    }
}

protocol Reusable: class {
    static var reuseIdentifier: String { get }
}

extension Reusable {
    static var reuseIdentifier: String { return String(describing: self) }
}
