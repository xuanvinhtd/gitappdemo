//
//  Configuration.swift
//  GitApp
//
//  Created by Vinh Ho Xuan on 5/23/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import UIKit

struct Configuration {
    static let baseUrl = "https://api.github.com"
    enum GITApi {
        case getUsers
        case getUserInfoWith(id:String)
        
        func url() -> URL {
            switch self {
            case .getUsers:
                return URL(string: "\(baseUrl)/users")!
            case let .getUserInfoWith(id):
                return URL(string: "\(baseUrl)/users/\(id)")!
            }
        }
    }
}


