//
//  GitAppTests.swift
//  GitAppTests
//
//  Created by Vinh Ho Xuan on 5/24/19.
//  Copyright © 2019 Vinh Ho Xuan. All rights reserved.
//

import XCTest

@testable import GitApp

class GitAppTests: XCTestCase {
    var userListViewModel:UserListViewModel!
    var userViewModel:UserViewModel!
    override func setUp() {
        super.setUp()
        userListViewModel = UserListViewModel()
    }
    
    override func tearDown() {
        userListViewModel = nil
        userViewModel = nil
        super.tearDown()
    }
    
    func testTotalUserDataLoaded() {
        let exp = expectation(description: "Check the total number user data get from server is 30 item")
        
        userListViewModel.loadSource {
          exp.fulfill()
        }
    
        waitForExpectations(timeout: 30) { [weak self] (error) in
            if let e = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(e)")
            }
            
            XCTAssertEqual(self?.userListViewModel.usersViewModel.count, 30, "The total number of all data is incorrect")
        }
    }
    
    func testAssertFollowingAndFollowerOfUser() {
        let exp = expectation(description: "Assert Following And Follower Of User")
        
        userListViewModel.loadSource {
            self.userViewModel = self.userListViewModel.usersViewModel[0]
            self.userViewModel.getDetail {
                exp.fulfill()
            }
        }
        
        waitForExpectations(timeout: 30) { [weak self] (error) in
            if let e = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(e)")
            }
            
            XCTAssertNotNil(self?.userViewModel.user.followers, "The followers is nil")
            XCTAssertNotNil(self?.userViewModel.user.following, "The following is nil")
        }
    }
}
